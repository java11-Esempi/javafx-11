module com.packt{
  //depends on the java.xml.bind module
  requires java.xml.bind;
  
  requires jdk.incubator.httpclient;

  //need this for Messages class to jdkbe available to java.xml.bind
  opens  com.packt to java.xml.bind;
  exports  com.packt to java.xml.bind;
}
