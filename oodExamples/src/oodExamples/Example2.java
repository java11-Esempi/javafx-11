package oodExamples;

import modello.Car;
import modello.Engine;
import modello.Vehicle;

public class Example2 {

	public static void main(String[] args) {
		double timeSec = 10.0;
		int horsePower = 246;
		int vehicleWeight = 4000;
		Engine engine = new Engine();
		engine.setHorsePower(horsePower);
		Vehicle vehicle = new Car(4, vehicleWeight, engine);
		System.out.println("Car speed (" + timeSec + " sec) = " + vehicle.getSpeedMph(timeSec) + " mph");
	}

}
