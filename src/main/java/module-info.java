/**
 * we can give a description to a module
 *
 * @since 1.1.1
 */
module com.example.javafx {

    requires transitive javafx.controls;
	requires javafx.media;
	requires javafx.fxml;
	requires javafx.web;

//    exports com.example.javafx to javafx.fxml, javafx.graphics;
//    opens com.example.javafx;
    opens com.example.javafx.FxMedia;
    opens com.example.javafx.FxWeb;
    opens com.example.javafx.FXML;
    opens com.example.javafx.ButtonController;
}

// exports com.example.javafx ;

// or 

// exports com.example.javafx to javafx.graphics;

// or

// opens com.example.javafx to javafx.graphics;