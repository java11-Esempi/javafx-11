package com.example.javafx.FxWeb;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class FxWebViewExample11 extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	public void start(Stage stage) {
		WebView webView = new WebView();
		WebEngine webEngine = webView.getEngine();
		// Load the Google web page
		webEngine.load("http://www.oracle.com");
		VBox root = new VBox(webView);
		// root.getChildrenUnmodifiable().add(webView);
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
